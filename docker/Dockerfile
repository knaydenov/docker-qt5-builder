FROM ubuntu:18.04

WORKDIR /src

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
      build-essential \
      perl \
      python \
      qt5-default \
      libssl-dev \
      git \
      wget \
      openjdk-11-jdk \
      && \
    rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/

# Install CMake
ARG CMAKE_VERSION=3.21.1
RUN cd /usr/local/src && \
    wget https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.tar.gz && \
    tar xf cmake-${CMAKE_VERSION}.tar.gz && \
    cd cmake-${CMAKE_VERSION} && \
    ./configure && \
    make && \
    make install && \
    rm -rf /usr/local/src/cmake-*

# Get sources
ARG QT5_VERSION=5.15.1
RUN cd /usr/local/src && \
    git clone --branch ${QT5_VERSION} -- https://code.qt.io/qt/qt5.git qt5 && \
    cd qt5 && \
    perl init-repository --module-subset=essential

# Build linux-g++
RUN mkdir /usr/local/src/qt5-build-linux-g++ && \
    cd /usr/local/src/qt5-build-linux-g++ && \
    /usr/local/src/qt5/configure \
      -prefix /usr/local/qt-${QT5_VERSION} \
      -recheck \
      -opensource \
      -nomake examples \
      -nomake tests \
      -no-opengl \
      -platform linux-g++ \
      -no-pch \
      -confirm-license \
      && \
    make -j$(nproc) && \
    rm -rf /usr/local/src/qt5-*

ENV QT_DIR=/usr/local/qt-${QT5_VERSION}


